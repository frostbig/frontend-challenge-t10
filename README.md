In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### LocalStorage

In the directory ['./data.md'] 
is the data to be added to LocalStorage with the names ["AllQuests"] and ["AllHeroes"] respectively.

### Work Time

['Time_Spent'] 2 months 1 day and 8 hours
['Hours_Worked'] 44 hours