import React, { Component } from 'react';
import './styles.css';
import logo from '../../../images/logo.png';
import title from '../../../images/title.png';
import heroIcon from '../../../images/hero_icon.png';
import questIcon from '../../../images/quest_icon.png';

export default class SideBar extends Component {
    state = {
        navi: []
    };
    componentDidMount() {
        this.setState({ navi: this.props.navi })
    }

    GoHero = e => {
        this.props.toggle("SidebarNavi", [true, false])
        this.setState({ navi: [true,false] })
    }
    GoQuest = e => {
        this.props.toggle("SidebarNavi", [false, true])
        this.setState({ navi: [false,true] })
    }

    render() {
        return (
            <div className="main__menu">
                <div className="main__menu--logo">
                    <img src={logo} alt="logo T10" id="logo" />
                    <img src={title} alt="title t10" id="title" />
                </div>

                <div className="main__menu--nav">
                    <ul>
                        <button 
                            id={"Selected__"+this.props.navi[0]}
                            onClick={this.GoHero}
                            className="buttonNavi" >
                            <img src={heroIcon} alt="hero icon" />
                            Heróis
                            </button>
                        <button
                        id={"Selected__"+this.props.navi[1]}
                            onClick={this.GoQuest}
                            className="buttonNavi" >
                            <img src={questIcon} alt="quest icon" />
                            Quests
                            </button>
                    </ul>
                </div>
            </div >
        )
    }
}