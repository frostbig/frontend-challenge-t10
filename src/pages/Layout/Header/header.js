import React, { Component } from 'react';
import './styles.css';

export default class Header extends Component {
    state = {
        tittle: "Heróis",
        heroNavi: [],
        questNavi: []
    };
    componentDidMount() {
        this.setState({ heroNavi: this.props.heroNavi });
        this.setState({ questNavi: this.props.questNavi });
    };

    GoOverall = e => {
        this.props.toggle("HeaderHeroNavi", [true, false, false]);
        this.setState({ heroNavi: [true, false, false] });
    };

    GoHero = e => {
        this.props.toggle("HeaderHeroNavi", [false, true, false]);
        this.setState({ heroNavi: [false, true, false] });
    };
    GoAddHero = e => {
        this.props.toggle("HeaderHeroNavi", [false, false, true]);
        this.setState({ heroNavi: [false, false, true] });
    };
    GoQuestOverall = e => {
        this.props.toggle("HeaderQuestNavi", [true, false]);
        this.setState({ questNavi: [true, false] });
    };
    GoAddQuest = e => {
        this.props.toggle("HeaderQuestNavi", [false, true]);
        this.setState({ questNavi: [false, true] });
    };

    render() {

        const propsHeader = this.props.header;
        let tittle = "Heróis";
        propsHeader[0] === false ? tittle = "Quest" : tittle = "Heróis";
        return (
            <div className="main__hero">
                <h1 >{tittle}</h1>
                <div className={"display__" + propsHeader[0]}>
                    <ul>
                        <button
                            onClick={this.GoOverall}
                            id={"Selected__" + this.state.heroNavi[0]}
                        >Overall</button>
                        <button
                            onClick={this.GoHero}
                            id={"Selected__" + this.state.heroNavi[1]}
                        >Heróis</button>
                        <button
                            onClick={this.GoAddHero}
                            id={"Selected__" + this.state.heroNavi[2]}
                        >Adicionar herói</button>
                    </ul>
                </div>
                <div className={"display__" + propsHeader[1]}>
                    <ul>
                        <button
                            onClick={this.GoQuestOverall}
                            id={"Selected__" + this.state.questNavi[0]}
                        >Overall</button>
                        <button
                            onClick={this.GoAddQuest}
                            id={"Selected__" + this.state.questNavi[1]}
                        >Criar quest</button>
                    </ul>
                </div>
            </div>

        )
    };
};