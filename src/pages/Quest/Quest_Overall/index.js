
import React, { Component } from 'react';
import './styles.css';
import point from '../../../images/point.png';

export default class Overall extends Component {
    state = {
        quests: [],
        status: "",
        numQuests: 0,
    }

    componentDidMount() {
        let quests = localStorage.getItem('AllQuests')
        quests ? this.setState({ quests: JSON.parse(quests) }) : console.log("NAO DNVDNV")
    }


    render() {
        const quests = this.state.quests
        const ChangeColor = e => {
            let status = e.target.value
            const data = status.split(",")
            const id = data[1] -1;
               const {quests} = this.state
               quests[id].status = data[0]
               this.setState({quest : quests})
               localStorage.setItem("AllQuests", JSON.stringify(this.state.quests))
        }
        return (
            <div className="overall">
                {quests.map(quest => (
                    <div className="main_quest" key={quest.id} id={"main_color_" + quest.status} >
                        <div id="main__left">
                            <h1>{quest.name}</h1>
                            <div id="main__left--date">
                                <div id="main__left--InitialDate">
                                    <h2>INICIO</h2>
                                    <h3>{quest.inicio}</h3>
                                </div>
                                <div id="main__left--FinalDate">
                                    <h2>FIM ESTIMADO</h2>
                                    <h3>{quest.fim}</h3>
                                </div>
                            </div>
                        </div>
                        <div id="main__mid">
                            <ul id="main__mid--ul">
                                HEROIS NESSA QUEST
                        {quest.herois.map(hero => (
                                    <li key={hero}><img src={point} alt="point"></img><h2 >{hero}</h2></li>
                                ))}
                            </ul>
                        </div>
                        <div id="main__rigth">
                            <select >
                                <option onClick={ChangeColor} value={["in_progress", quest.id]}>EM ANDAMENTO</option>
                                <option onClick={ChangeColor} value={["completed", quest.id]}>CONCLUÍDA NO DIA</option>
                                <option onClick={ChangeColor} value={["early", quest.id]}>ANTECIPADA</option>
                                <option onClick={ChangeColor} value={["late", quest.id]}>ATRAZADA</option>
                            </select>
                        </div>
                    </div>

                ))}

            </div>
        )
    }
}