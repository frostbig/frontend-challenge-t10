import React, { Component } from 'react';
import './styles.css';
import exclude from '../../../images/exclude.png'
import openAngle from '../../../images/angle-open.png'
import closeAngle from '../../../images/angle-close.png'

export default class AddQuest extends Component {
    state = {
        open: false,
        heroList: [],
        questList: [],
        questName: "",
        questDateInit: new Date().getDate(),
        questDateEnd: new Date().getDate(),
        questDateMax: new Date().getDate(),
        heroesSelected: []
    }
    componentDidMount() {
        let heroes = localStorage.getItem('AllHeroes')
        let quests = localStorage.getItem('AllQuests')
        quests ? this.setState({ questList: JSON.parse(quests) }) : console.log("NAODNVDNVDNV")
        heroes ? this.setState({ heroList: JSON.parse(heroes) }) : console.log("NAODNVDNVDNV")
    }
    QuestNameChange = e => {
        this.setState({ questName: e.target.value })
    }
    QuestDateInitChange = e => {
        this.setState({ questDateInit: e.target.value })
    }
    QuestDateMaxChange = e => {
        this.setState({ questDateMax: e.target.value })
    }
    QuestDateEndChange = e => {
        this.setState({ questDateEnd: e.target.value })
    }
    convertDate = (Date) => {
        return Date[2] + "/" + Date[1] + "/" + Date[0]
    }

    AddQuest = e => {
        let i, j;
        const { heroList, questList, heroesSelected, questDateEnd, questDateInit, questDateMax, questName } = this.state
        if ( questName === "" || questDateEnd === 10 || questDateInit === 10 || questDateMax === 10){console.log("NULL"); return }
        const init = this.convertDate(questDateInit.split("-"))
        const end = this.convertDate(questDateEnd.split("-"))
        const max = this.convertDate(questDateMax.split("-"))
        const object_Quest = {
            id: questList.length + 1,
            name: questName,
            inicio: init,
            fim: end,
            herois: heroesSelected,
            max: max,
            status: "in_progress"
        }
        localStorage.setItem("AllQuests", JSON.stringify([...questList, object_Quest]))
        for (i = 0; i < heroesSelected.length; i++) {
            for (j = 0; j < heroList.length; j++) {
                if (heroList[j].name === heroesSelected[i]) {
                    console.log(heroList[j].name, heroesSelected[i])

                }
            }
        }
        window.location.reload()
    }


    Selected = (e) => {
        const heroes = this.state.heroesSelected
        for (let i = 0; i < heroes.length; i++) {
            if (e.target.value === heroes[i]) { return }

        }
        this.setState({ heroesSelected: [...heroes, e.target.value] })
    }
    ExcludeHero = (e) => {
        const heroes = this.state.heroesSelected
        for (let i = 0; i < heroes.length; i++) {
            if (e.target.alt === heroes[i] || e.target.value === heroes[i]) {
                heroes.splice(i, 1)
            }
        }
        this.setState({ heroesSelected: heroes })

    }

    ToggleAngle = () => {
        const angle = document.getElementById("selectHero")
        if (this.state.open === true) {
            angle.style.backgroundImage = "url(" + openAngle + ")";
            this.setState({ open: false })
            return
        }
        angle.style.backgroundImage = "url(" + closeAngle + ")";
        this.setState({ open: true })
    }

    render() {

        const { heroList, heroesSelected, questName, questDateEnd, questDateInit, questDateMax } = this.state
        return (
            <div className="Container">
                <div className="Container__left">
                    <h2>NOME DA QUEST</h2>
                    <input onChange={this.QuestNameChange} value={questName} type="text" id="Container__rigth--name" ></input>

                    <h2>ENTREGA ESTIMADA</h2>
                    <input onChange={this.QuestDateEndChange} type="date" id="Container__rigth--date" value={questDateEnd} required></input>

                    <h2>HERÓIS NESSA QUEST</h2>
                    <select id="selectHero" onClick={this.ToggleAngle} >
                        {heroList.map(hero => (
                            <option onClick={this.Selected} key={hero.name} value={hero.name}>{hero.name}</option>
                        ))}
                    </select>
                    <ul id="SelectedList">
                        {heroesSelected.map(selected => (
                            <li className="SelectedHero" id={"Selected__" + selected} key={"Selected__" + selected}>
                                <h2 id="SelectedHero__text">{selected}</h2>
                                <button value={selected} onClick={this.ExcludeHero} ><img src={exclude} alt={selected} />
                                </button>
                            </li>
                        ))
                        }
                    </ul>
                </div>
                <div className="Container__rigth">
                    <h2>INÍCIO DA QUEST</h2>
                    <input onChange={this.QuestDateInitChange} type="date" data-date-format="DD MMMM YYYY" id="Container__rigth--date" value={questDateInit} required></input>

                    <h2>ENTREGA MÁXIMA</h2>
                    <input onChange={this.QuestDateMaxChange} type="date" id="Container__rigth--date" value={questDateMax} required></input>

                    <button onClick={this.AddQuest} id="Container__rigth--button">CRIAR</button>
                </div>
            </div>
        )
    }
}