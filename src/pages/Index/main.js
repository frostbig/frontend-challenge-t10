import React, { Component } from 'react';
import './styles.css';
import Sidebar from '../Layout/SideBar/SideBar';
import Header from '../Layout/Header/header.js';
import Content from '../Content/index';

export default class HighOrderComponent extends Component {
    state = {
        sidebarNavi: [true, false],
        headerHeroNavi: [true, false, false],
        headerQuestNavi: [true, false]
    }
    ToggleNavi = (nameChange, newData) => {
        if (nameChange === "SidebarNavi") {
            this.setState({ sidebarNavi: newData })
            const navi = this.state.sidebarNavi;
            return navi;
        }
        if (nameChange === "HeaderHeroNavi") {
            this.setState({ headerHeroNavi: newData })
            const navi = this.state.headerHeroNavi;
            return navi;
        }
        if (nameChange === "HeaderQuestNavi") {
            this.setState({ headerQuestNavi: newData })
            const navi = this.state.headerQuestNavi;
            return navi;
        }
    }
    render() {
        return (
            <div className="HighOrder">
                <Sidebar navi={this.state.sidebarNavi} toggle={this.ToggleNavi} />
                <Header header={this.state.sidebarNavi} heroNavi={this.state.headerHeroNavi} questNavi={this.state.headerQuestNavi} toggle={this.ToggleNavi} />
                <Content sideNavi={this.state.sidebarNavi} heroNavi={this.state.headerHeroNavi} questNavi={this.state.headerQuestNavi} />
            </div>
        )
    }
}
