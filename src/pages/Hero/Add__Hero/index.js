import React, { Component } from 'react';
import './styles.css'

export default class AddHero extends Component {
    state = {
        heroList: [],
        newHero: "",
        heroSpec: "",
        buttonDesabled: false
    }
    componentDidMount() {
        let heroes = localStorage.getItem('AllHeroes')
        heroes ? this.setState({ heroList: JSON.parse(heroes) }) : console.log("NAO DNV")
    }
    HeroNameChange = e => {
        this.setState({ newHero: e.target.value })
    }
    HeroSpecChange = e => {
        this.setState({ heroSpec: e.target.value })
    }
    AddHero = () => {
        const { heroList, newHero, heroSpec } = this.state
        const heroData = {
            name: newHero.toUpperCase(),
            specialty: heroSpec.toLowerCase(),
            xp: 0,
            quests: []
        }

        if (heroData.name && heroData.specialty !== "") {
            const heroes = [...heroList, heroData]
            localStorage.setItem('AllHeroes', JSON.stringify(heroes))
            window.location.reload()
        }
    }
    ButtonEnabled = () => {
        const { heroSpec, newHero } = this.setState
        if (heroSpec && newHero !== "") {
            this.setState({ buttonDesabled: false })
        }
    }

    render() {
        const { newHero, heroSpec } = this.state

        return (
            <div className="main__content" >
                <ul>
                    <li><h2>NOME DO HERÓI</h2>
                        <input required type="text" placeholder="" id="hero__name--input" value={newHero} onChange={this.HeroNameChange}></input>
                    </li>
                    <li><h2>ESPECIALIDADE</h2>
                        <input required type="text" placeholder="" id="hero__value--input" value={heroSpec} onChange={this.HeroSpecChange}></input>
                        <button type="button" className="submit__button--Hero" onClick={this.AddHero}>ADICIONAR</button>
                    </li>
                </ul>
            </div>
        )
    }
}