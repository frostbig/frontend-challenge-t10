import React, { Component } from 'react';
import './styles.css';
import dev_icon from '../../../images/dev_icon.png';

export default class Heroes extends Component {
    state = {
        allHeroes: []
    }
    componentDidMount() {
        let heroes = localStorage.getItem('AllHeroes')
        heroes ? this.setState({ allHeroes: JSON.parse(heroes) }) : heroes = []
    }

    render() {
        const { allHeroes } = this.state

        return (
            <div id="heroes__list">
                {allHeroes.map(hero => (
                    <div key={hero.name} className="heroes" >
                        <img className="heroes__icon" src={dev_icon} alt="dev icon" />
                        <ul>
                            <li id="heroes__icon--name">{hero.name}</li>
                            <li>{hero.specialty}</li>
                        </ul>
                    </div>
                ))}
            </div>


        )
    }
}