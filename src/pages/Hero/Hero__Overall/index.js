import React, { Component } from 'react';
import './styles.css';
import point from '../../../images/point.png';
import dev_icon from '../../../images/dev_icon.png';
import bar from '../../../images/bar.png';

export default class Overall extends Component {
    state = {
        hero: [],
        quest: [],
        heroQuest: []
    }
    componentDidMount() {
        let heroes = localStorage.getItem('AllHeroes')
        let quest = localStorage.getItem('AllQuests')
        heroes ? this.setState({ hero: JSON.parse(heroes) }) : console.log("NAO")
        quest ? this.setState({ quest: JSON.parse(quest) }) : console.log("NONO")
    }

    render() {
        let j, k, l, i;
        const { hero, quest } = this.state
        i = 0;
        const InsideXp = (XP) => {
            return XP > 500 ? "" : "False"
        }
        const OutsideXp = (XP) => {
            return XP < 500 ? "" : "False"
        }
        for (j = 0; j < hero.length; j++) {
            for (k = 0; k < quest.length; k++) {
                const quests = quest[k].herois
                for (l = 0; l < quests.length; l++) {
                    if (quests[l] === hero[j].name) {
                        hero[j].quests = [...hero[j].quests, quest[k].name]
                    }
                }
            }
        }
        return (
            <div id="main__Overall">
                {hero.map(data => (
                    <div key={data.name} className="overall">
                        <div id="overall__name" ref="2">
                            <div id="overall__name--hero" >
                                <img src={dev_icon} alt="hero icon"></img>
                                <h2>{data.name}</h2>
                            </div>
                            <div id="overall__name--xp">
                                <h2 id="overall__name--xpText">XP</h2>
                                <img src={bar} alt="bar"></img>
                                <div id={"overall__name--Bar" + data.xp}>
                                    <h2 id={"overall__name--BarInside" + InsideXp(data.xp)}>{data.xp}</h2>
                                </div>
                                <h2 id={"overall__name--BarOutside" + OutsideXp(data.xp)}>{data.xp}</h2>
                            </div>
                        </div>
                        <div id="list">
                            <ul>
                                QUESTS EM ANDAMENTO
                                    {data.quests.map(quest => (
                                    <li key={++i}>
                                        <img src={point} alt="point"></img>
                                        <h2>{quest}</h2>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}