import React, { Component } from 'react';
import './styles.css'
import AddHero from '../Hero/Add__Hero/index';
import Heroes from '../Hero/Heros/index';
import Overall from '../Hero/Hero__Overall/index';
import QuestOverall from '../Quest/Quest_Overall';
import AddQuest from '../Quest/Create_Quest';

export default class Content extends Component {
    componentDidMount() {
        this.setState({ sideBar: this.props.sideNavi })
        this.setState({ heroNavi: this.props.heroNavi })
        this.setState({ questNavi: this.props.questNavi })
    }
    render() {
        return (
            <div id="main">
                <div className={"display__" + this.props.sideNavi[0]}>
                    <div className={"display__" + this.props.heroNavi[0]}><Overall /></div>
                    <div className={"display__" + this.props.heroNavi[1]}><Heroes /></div>
                    <div className={"display__" + this.props.heroNavi[2]}><AddHero /></div>
                </div>
                <div className={"display__" + this.props.sideNavi[1]}>
                    <div className={"display__" + this.props.questNavi[0]}><QuestOverall /></div>
                    <div className={"display__" + this.props.questNavi[1]}><AddQuest /></div>
                </div>
            </div>
        )
    }
}