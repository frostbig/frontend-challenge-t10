import React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import RoutesInfo from './constants/routes';
let id = 0;
const getRoutes = RoutesInfo.map(elm => < Route {...elm} key={"keyRoutes_"+id+1} />)

const Routes = () =>
    <BrowserRouter>
        <Switch>
            {getRoutes}
        </Switch>
    </BrowserRouter>

export default Routes;        