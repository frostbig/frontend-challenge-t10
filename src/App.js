import React from 'react';
import Routes from './Routes.js';
import './styles/global.css';

const App = () => <Routes />
export default App;
